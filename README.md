---
title: Coronavirus student project
---

A PB051 bioinformatics project at the Faculty of Informatics at MU in Brno.

### Goals (with web pages to create):

1. Collect as many coronavirus sequences as possible (Page 1 - Sequence data)
1. Using HMMER create a profile-HMM of coronavirus sequence and/or its important subsequences (Page 2 - HMM Alignment)
1. Find other related sequences in public sequence databases (Page 3 - Related sequences)
1. Create a labeled phylogenetic tree of the aligned sequences (Page 4 - Phylogenetic trees)
1. Phylogeographic analysis (Page 5 - Phylogeography)
1. Time series analysis (Page 6 - Time series)

### Setup

First install required package in R:

- pandoc (May differ by distro)

```R
install.packages("rmarkdown")

# To render a website
rmarkdown::render_site()
```


### Organizational links

[Doodle poll](https://doodle.com/poll/dmgb7n3n2wdp69hc)

[Course discussion forum](https://is.muni.cz/auth/discussion/predmetove/fi/jaro2020/PB051/?fakulta=1433;obdobi=7644;predmet=1196217)

## Sources of relevant data

[NCBI: SARS-CoV-2 (Severe acute respiratory syndrome coronavirus 2) Sequences](https://www.ncbi.nlm.nih.gov/genbank/sars-cov-2-seqs/)

[University of Oxford: Our World in Data COVID-19 page](https://ourworldindata.org/coronavirus#up-to-date-n-co-v-2019-data-working-group-data)

[WHO Situation reports](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports/)

[John Hopkins University Data and Links](https://github.com/CSSEGISandData/COVID-19)

[Italian data](https://github.com/pcm-dpc)

## Software

[HMMER 3.0](http://hmmer.org/)

## References

[Wikipedia background information](https://en.wikipedia.org/wiki/Severe_acute_respiratory_syndrome_coronavirus_2)

[Cohen (2020): Mining coronavirus genomes for clues to the outbreak’s origins](https://www.sciencemag.org/news/2020/01/mining-coronavirus-genomes-clues-outbreak-s-origins)

[Analysis of Belgian cases in R](https://r-bloggers.com/corona-in-belgium/amp)

[Quick NGS data analysis](https://medium.com/analytics-vidhya/data-analysis-of-coronavirus-and-its-host-basic-bioinformatics-methods-with-code-1eb0b103270)

[Data integration, manipulation and visualization of phylogenetic trees](https://yulab-smu.github.io/treedata-book/)

[GIT Handbook](https://guides.github.com/introduction/git-handbook/)

[RMarkdown information](https://rmarkdown.rstudio.com/lesson-1.html)

[These guys were faster than us](https://nextstrain.org/ncov) :-)

