#!/bin/bash

# Specify server:path (in scp format) to where the coronavirus site is being deployed using $1 bash variable
# Example: 
# ./deploy.sh lexa@helix.fi.muni.cz:/mnt/lexa/public_html/coronavirus
# Note:
# To deploy up-to-date static webpages, run RMarkdown render_site() command in R first

# poor man's help message
if [ -z "$1" ]; then
       	echo "Warning: use scp compliant server:path argument to specify deployment target"
	exit 
fi

#cp *.html /mnt/lexa/public_html/pa055/.
#cp -R images /mnt/lexa/public_html/pa055/.
#cp -R site_libs /mnt/lexa/public_html/pa055/.
#cp -R lesson1_files /mnt/lexa/public_html/pa055/.

scp *.html $1/.
scp -r images $1/.
scp -r site_libs $1/.
scp -r page*_files $1/.

